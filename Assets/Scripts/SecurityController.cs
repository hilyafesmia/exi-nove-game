﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SecurityController : MonoBehaviour {

    public float moveSpeed = 8;

    private Animator anim;
    private Rigidbody2D myRigidBody;

    private bool securityMoving;

    private bool moving;

    private Vector2 lastMove;
    private Vector2 move;

    public float timeBetweenMove;
    private float timeBetweenMoveCounter;
    public float timeToMove;
    private float timeToMoveCounter;

    private Vector2 moveDirection;
    private Vector2 follow;

    public float waitToReload;
    private bool reloading;
    private GameObject thePlayer;

    public float lookRadius = 10f;
    private Transform target;
    NavMeshAgent agent;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        myRigidBody = GetComponent<Rigidbody2D>();

        timeBetweenMoveCounter = Random.Range(timeBetweenMove * 0.75f, timeBetweenMove * 1.25f);
        timeToMoveCounter = Random.Range(timeToMove * 0.75f, timeToMove * 1.25f);

        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        agent = GetComponent<NavMeshAgent>();

    }
	
	// Update is called once per frame
	void Update () {

        securityMoving = false;
        float distance = Vector3.Distance(target.position, transform.position);

        if (distance <= lookRadius)
        {
            //Vector2 velocity = (target.position - transform.position).normalized * moveSpeed;
            //myRigidBody.velocity = velocity;
            move.x = target.position.x - transform.position.x;
            move.y = target.position.y - transform.position.y;
            follow = Vector2.MoveTowards(transform.position, target.position, moveSpeed  * Time.deltaTime);
            transform.position = follow;
            moving = true;
            securityMoving = true;
        }
        else
        {
            if (moving)
            {
                timeToMoveCounter -= Time.deltaTime;
                myRigidBody.velocity = moveDirection;
                if (timeToMoveCounter < 0f)
                {
                    moving = false;
                    securityMoving = true;
                    timeBetweenMoveCounter = Random.Range(timeBetweenMove * 0.75f, timeBetweenMove * 1.25f);
                }
            }
            else
            {
                timeBetweenMoveCounter -= Time.deltaTime;
                myRigidBody.velocity = Vector2.zero;
                if (timeBetweenMoveCounter < 0f)
                {
                    moving = true;
                    securityMoving = false;
                    timeToMoveCounter = Random.Range(timeToMove * 0.75f, timeToMove * 1.25f);
                    lastMove.x = Random.Range(-1f, 1f);
                    lastMove.y = Random.Range(-1f, 1f);
                    move = lastMove;
                    moveDirection = new Vector2(lastMove.x * moveSpeed, lastMove.y * moveSpeed);
                }
            }
        }
        

        if (reloading)
        {
            waitToReload -= Time.deltaTime;
            if(waitToReload < 0)
            {
                Application.LoadLevel(Application.loadedLevel);
                thePlayer.SetActive(true);
            }
        }

        //Input.GetAxisRaw("Horizontal") <-- Ini ganti
        //Input.GetAxisRaw("Vertical") <-- Ini ganti
        //Tambahin kapan update securityMoving sama lastMove
        anim.SetFloat("MoveX", move.x);
        anim.SetFloat("MoveY", move.y);
        anim.SetBool("SecurityMoving", moving);
        anim.SetFloat("LastMoveX", lastMove.x);
        anim.SetFloat("LastMoveY", lastMove.y);

    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.name == "player")
        {
            other.gameObject.SetActive(false);
            reloading = true;
            thePlayer = other.gameObject;
        }
    }
}
