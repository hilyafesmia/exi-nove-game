﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteract : MonoBehaviour {

    public GameObject currentInterObj = null;
    public InteractionObject currentInterObjScript = null;
    public Inventory inventory;

	void Update () {
		if(Input.GetButtonDown ("Interact") && currentInterObj)
        {
            //Check to see whether the object is to be stored in inventory
            if(currentInterObjScript.inventory){
                inventory.AddItem(currentInterObj);
                currentInterObj = null;
            }

            //Check to see whether the object can be opened
            if (currentInterObjScript.openable)
            {
                //Check to see whether the object is locked
                if(currentInterObjScript.locked)
                {
                    //Check to see whether we have the object needed to unlock
                    //Search inventory for the item needed
                    // if found => unlock object
                    // not found ==> remain locked
                    if(inventory.FindItem(currentInterObjScript.itemNeeded))
                    {
                        currentInterObjScript.locked = false;
                        Debug.Log(currentInterObj.name + " terbuka!");
                        currentInterObjScript.Open();
                        inventory.DeleteItem(currentInterObjScript.itemNeeded);
                        currentInterObj = null;
                    }
                    else
                    {
                        Debug.Log(currentInterObj.name + " masih terkunci! " + currentInterObjScript.itemNeeded.name + " tidak ditemukan");
                        currentInterObj = null;
                    }
                }
                else
                {
                    //object is not locked, open the object
                    Debug.Log(currentInterObj.name + " terbuka!");
                    currentInterObjScript.Open();
                    currentInterObj = null;
                }
            }
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag ("interObject"))
        {
            Debug.Log(other.name);
            currentInterObj = other.gameObject;
            currentInterObjScript = currentInterObj.GetComponent<InteractionObject>();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("interObject"))
        {
            if(other.gameObject == currentInterObj)
            {
                currentInterObj = null;
            }
            
        }
    }
}
